import requests
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
import argparse
import time

def print_headline(headline):
    print(20*"-" + headline + 20*"-")

def request(session, address, port, route, json):
    r = session.post(f"http://{address}:{port}{route}", 
            json=json)
    return r

def main(address, port):
    # handling situations, when server is not running (yet)
    session = requests.Session()
    retry = Retry(connect=10, backoff_factor=1)
    adapter = HTTPAdapter(max_retries=retry)
    session.mount('http://', adapter)
    time.sleep(5)

    basic_text = 'I have a sentence to test'
    ner_text = "Microsoft was looking at buying U.K. startup for $1 billion"
    # list of requests to send
    parameters = [
        ('SENTIMENT', '/sentiment', {
            'text': basic_text
        }),
        ('POS TAGGING', '/pos-tagging', {
            'text': basic_text
        }),
        ('NER', '/ner', {
            'text': ner_text
        }),
        ('LEMMATIZATION', '/lemmatization', {
            'text': basic_text
        }),
        ('TOKENIZATION', '/tokenization', {
            'text': basic_text
        }),
        ('SIMILARITY', '/similarity', {
            'text1': basic_text + "1",
            'text2': basic_text + "2"
        }),
    ]

    for headline, route, json in parameters:
        print_headline(headline)
        print(f"Input: {json}")
        response = request(session, address, port, route, json)
        print(response.json())

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--address", help="Address to connect", 
                            type=str, default="127.0.0.1")
    parser.add_argument("--port", help="Port to use", 
                            type=int, default=5000)
    args = parser.parse_args()
    main(args.address, args.port)