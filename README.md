<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Repository</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project

This is preinterview task for Ecovis company. The general idea was to prepare API interface for some basic machine learning (NLP) functionalities taken from pretrained, ready to use model.

Project contains two main directories with two main components: 
- /server: server serving mentioned API 
- /client: simple client. 

There is also subdirectory /tests, as server endpoint got some basic unit tests prepared.

Both modules are containerized in order to make runnig the repository as straightforward as possible.

<p align="right">(<a href="#top">back to top</a>)</p>



### Built With

Here you can find major packages and technologies used for this project.

* flask
* flask-restful
* spaCy
* docker
* pytest

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- GETTING STARTED -->
## Getting Started

### Prerequisites

Before using the repository make sure that you have **_docker_** and **_docker-compose_** installed on your machine. 

**Important:** You may need to have admin rights on linux machine to use docker commands (or current user added to docker group). So you may need to add `sudo` before docker related commands

### Installation

These are few steps you need to take to prepare environment and make program ready to use.

1. Clone the repo
   ```sh
   git clone https://gitlab.com/pysqaty/ecovis-interview-task
   ```
2. Go to the root of the cloned project
   ```sh
   cd <PATH_TO_ROOT>
   ```
3. Build docker images (**Note** that tests are performed during this process and succesfull creation of environment means that all tests passed and everything works as expected.)
   ```sh
   docker-compose build
   ```

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- USAGE EXAMPLES -->
## Usage

In order to run the program you need to take few simple steps.
1. Go to the root of the cloned project
   ```sh
   cd <PATH_TO_ROOT>
   ```
2. Run containers
   ```sh
   docker-compose up
   ```

If you want to stop the containers use `CTRL+C` shortcut and then command `docker-compose down` to clean the containers.

<p align="right">(<a href="#top">back to top</a>)</p>


<!-- CONTACT -->
## Contact

Dawid Mastalerz - dawid.mastalerz27@gmail.com

Project Link: [https://gitlab.com/pysqaty/ecovis-interview-task](https://gitlab.com/pysqaty/ecovis-interview-task)

<p align="right">(<a href="#top">back to top</a>)</p>
