from flask import Flask
from flask_restful import Api
from resources.sentiment.sentiment import Sentiment
from resources.pos_tagging.pos_tagging import POSTagging
from resources.ner.ner import NER
from resources.lemmatization.lemmatization import Lemmatization
from resources.tokenization.tokenization import Tokenization
from resources.similarity.similarity import Similarity

def create_app():
    app = Flask(__name__)
    _add_resources(app)
    return app

# binding resources to endpoints
def _add_resources(app):
    api = Api(app)
    api.add_resource(Sentiment, '/sentiment')
    api.add_resource(POSTagging, '/pos-tagging')
    api.add_resource(NER, '/ner')
    api.add_resource(Lemmatization, '/lemmatization')
    api.add_resource(Tokenization, '/tokenization')
    api.add_resource(Similarity, '/similarity')