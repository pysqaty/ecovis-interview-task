from flask_restful import Resource, reqparse
from models.base_model import model
import spacy
from parsers.single import parser

class NER(Resource):
    def post(self):
        args = parser.parse_args()
        text = args['text']
        if not isinstance(text, str):
            return "String data expected", 400
        doc = model(text)
        result = []
        for t in doc.ents:
            result.append({
                "token": t.text, 
                "label": t.label_,
                "desc": str(spacy.explain(t.label_))})
        return {'ner': result}