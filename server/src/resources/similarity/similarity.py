from flask_restful import Resource, reqparse
from models.base_model import model
from parsers.double import parser

class Similarity(Resource):
    def post(self):
        args = parser.parse_args()
        text1 = args['text1']
        text2 = args['text2']
        doc1 = model(text1)
        doc2 = model(text2)
        score = doc1.similarity(doc2)
        return {'similarity': score}