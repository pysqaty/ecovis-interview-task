from flask_restful import Resource, reqparse
from models.base_model import model
from parsers.single import parser

class Sentiment(Resource):
    def post(self):
        args = parser.parse_args()
        text = args['text']
        doc = model(text)
        score = doc._.polarity
        return {'sentiment': score}