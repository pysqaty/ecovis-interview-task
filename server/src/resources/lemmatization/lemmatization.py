from flask_restful import Resource, reqparse
from models.base_model import model
from parsers.single import parser

class Lemmatization(Resource):
    def post(self):
        args = parser.parse_args()
        text = args['text']
        doc = model(text)
        result = []
        for t in doc:
            result.append({
                "token": t.text, 
                "lemma": t.lemma_
                })
        return {'lemmatization': result}, 200