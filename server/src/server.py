import sys
import os
sys.path.append(os.getcwd())

from app import create_app
import argparse

app = create_app()

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--address", help="Address to connect", 
                            type=str, default="0.0.0.0")
    parser.add_argument("--port", help="Port to use", 
                            type=int, default=5000)
    args = parser.parse_args()
    app.run(host=args.address, port=args.port)