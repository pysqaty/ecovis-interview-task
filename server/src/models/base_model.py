import spacy
from spacytextblob.spacytextblob import SpacyTextBlob

# small English model from spacy package with additional pipe used for sentiment classification
model = spacy.load('en_core_web_sm')
model.add_pipe('spacytextblob')