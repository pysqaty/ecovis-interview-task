from flask_restful import reqparse

# simple parser used for endpoints operating on two pieces of text
parser = reqparse.RequestParser()
parser.add_argument('text1', required=True, nullable=False,
            type=str, help='First text for the model to be fed')
parser.add_argument('text2', required=True, nullable=False,
            type=str, help='Second text for the model to be fed')