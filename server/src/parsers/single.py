from flask_restful import reqparse

# the most basic parser used for endpoints processing single part of text
parser = reqparse.RequestParser()
parser.add_argument('text', required=True, nullable=False,
            type=str, help='Text for the model to be fed')