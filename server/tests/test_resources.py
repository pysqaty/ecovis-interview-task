import pytest

from app import create_app

#----------------------- FIXTURES -----------------------

# app instance used for tests
@pytest.fixture()
def app():
    app = create_app()
    app.config.update({
        "TESTING": True,
    })
    yield app

# client used for tests
@pytest.fixture()
def client(app):
    return app.test_client()

#----------------------- TESTS -----------------------

# lemmatization endpoint testing
@pytest.mark.parametrize(
    "key, value, response_code, xresult",
    [
        ('text', 'was', 200, ['be']),
        ('text', 'I was', 200, ['I', 'be'])
    ]
)
def test_lemmatization(client, key, value, response_code, xresult):
    response = client.post("/lemmatization", json={
        key: value
    })
    assert response.status_code == response_code
    if response.status_code != 200:
        return
    assert 'lemmatization' in response.json
    assert len(response.json['lemmatization']) == len(xresult)
    for i in range(0, len(xresult)):
        result = response.json['lemmatization'][i]
        assert 'token' in result
        assert 'lemma' in result
        assert result['token'] in value
        assert result['lemma'] == xresult[i]

# ner endpoint testing
@pytest.mark.parametrize(
    "key, value, response_code, xresult",
    [
        ('text', 'Test', 200, []),
        ('text', 'Microsoft was looking at buying U.K. startup for $1 billion', 200, ['ORG', 'GPE', 'MONEY']),
    ]
)
def test_ner(client, key, value, response_code, xresult):
    response = client.post("/ner", json={
        key: value
    })
    assert response.status_code == response_code
    if response.status_code != 200:
        return
    assert 'ner' in response.json
    assert len(response.json['ner']) == len(xresult)
    for i in range(0, len(xresult)):
        result = response.json['ner'][i]
        assert 'token' in result
        assert 'label' in result
        assert 'desc' in result
        assert result['token'] in value
        assert result['label'] == xresult[i]

# sentiment endpoint testing
@pytest.mark.parametrize(
    "key, value, response_code, predicate",
    [
        ('text', 'Love', 200, lambda x: x > 0.0),
        ('text', 'Horrible', 200, lambda x: x < 0.0),
        ('text', '', 200, lambda x: abs(x) < 1e-6),
    ]
)
def test_sentiment(client, key, value, response_code, predicate):
    response = client.post("/sentiment", json={
        key: value
    })
    assert response.status_code == response_code
    if response.status_code != 200:
        return
    assert 'sentiment' in response.json
    result = response.json['sentiment']
    assert predicate(result)

# tokenization endpoint testing
@pytest.mark.parametrize(
    "key, value, response_code",
    [
        ('text', 'Sample sentence for tokenization', 200),
        ('text', 'World', 200)
    ]
)
def test_tokenization(client, key, value, response_code):
    response = client.post("/tokenization", json={
        key: value
    })
    assert response.status_code == response_code
    if response.status_code != 200:
        return
    assert 'tokenization' in response.json
    for i in range(0, len(response.json['tokenization'])):
        result = response.json['tokenization'][i]
        assert 'token' in result
        assert result['token'] in value

# similarity endpoint testing
@pytest.mark.parametrize(
    "json, response_code, predicate",
    [
        ({'text1': 'Test sentence.', 'text2': 'Test sentence.'}, 200, lambda x: (abs(x)-1) < 1e-6),
        ({'text1': 'Test sentence.', 'text2': 'Something totally different'}, 200, lambda x: x >= 0.0 and x < 1.0),
    ]
)
def test_similarity(client, json, response_code, predicate):
    response = client.post("/similarity", json=json)
    assert response.status_code == response_code
    if response.status_code != 200:
        return
    assert 'similarity' in response.json
    result = response.json['similarity']
    assert predicate(result)

# pos tagging endpoint testing
@pytest.mark.parametrize(
    "key, value, response_code, xresult_pos, xresult_tag",
    [
        ('text', 'I have a sentence to test', 200, 
            ['PRON', 'VERB', 'DET', 'NOUN', 'PART', 'VERB'],
            ['PRP', 'VBP', 'DT', 'NN', 'TO', 'VB']),
    ]
)
def test_pos_tagging(client, key, value, response_code, xresult_pos, xresult_tag):
    response = client.post("/pos-tagging", json={
        key: value
    })
    assert response.status_code == response_code
    if response.status_code != 200:
        return
    assert 'pos_tagging' in response.json
    assert len(response.json['pos_tagging']) == len(xresult_pos)
    for i in range(0, len(xresult_pos)):
        result = response.json['pos_tagging'][i]
        assert 'token' in result
        assert 'pos' in result
        assert 'tag' in result
        assert result['token'] in value
        assert result['pos'] == xresult_pos[i]
        assert result['tag'] == xresult_tag[i]

# incorrect data sent to several endpoint testign
@pytest.mark.parametrize(
    "route, json, response_code",
    [
        ('/lemmatization', {'data': 'Wrong json sent'}, 400),
        ('/lemmatization', {'text': None}, 400),
        ('/ner', {'data': 'Wrong json sent'}, 400),
        ('/ner', {'text': None}, 400),
        ('/sentiment', {'data': 'Wrong json sent'}, 400),
        ('/sentiment', {'text': None}, 400),
        ('/pos-tagging', {'data': 'Wrong json sent'}, 400),
        ('/pos-tagging', {'text': None}, 400),
        ('/tokenization', {'data': 'Wrong json sent'}, 400),
        ('/tokenization', {'text': None}, 400),
        ('/similarity', {'data': 'Wrong json sent'}, 400),
        ('/similarity', {'text1': None}, 400),
        ('/similarity', {'text2': None}, 400),
        ('/similarity', {'text1': None, 'text2': "Correct"}, 400),
        ('/similarity', {'text1': "Correct", 'text2': None}, 400),
        ('/similarity', {'text1': None, 'text2': None}, 400),
    ]
)
def test_parsing(client, route, json, response_code):
    response = client.post(route, json=json)
    assert response.status_code == response_code